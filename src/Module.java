import java.util.Vector;

public class Module
{
    private String namespace_;
    private final IClassSemantic cls_;
    private Vector<IInterface> interfaces_ = new Vector<>();
    private Vector<IDependency> dependencies_ = new Vector<>();

    Module(String namespace, IClassSemantic cls)
    {
        namespace_ = namespace;
        cls_ = cls;
    }

    void addInterface(IInterface ifc)
    {
        interfaces_.add(ifc);
    }

    void addDependency(IDependency dependency)
    {
        dependencies_.add(dependency);
    }

    public String toString()
    {
        String openNamespace = "namespace " + namespace_ + "\n" + "{\n";
        String closeNamespace = "\n}";
        return openNamespace + cls_.openScope(interfaces_)
                + cls_.constructor(dependencies_) +cls_.closeScope() + closeNamespace;
    }
}