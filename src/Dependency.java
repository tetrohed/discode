interface IDependency
{
    String toString();
}

public class Dependency implements IDependency
{
    private final IInterface ifc_;
    private final String name_;

    public Dependency(IInterface ifc, String name)
    {
        ifc_ = ifc;
        name_ = name;
    }

    @Override
    public String toString()
    {
        return ifc_.toString() + " " + name_;
    }
}
