interface IUnitTest
{
    String toString();
}

public class UnitTest implements IUnitTest
{
    private String namespace_;
    private IClassSemantic cls_;

    UnitTest(String namespace, IClassSemantic cls)
    {
        namespace_ = namespace;
        cls_ = cls;
    }

    @Override
    public String toString()
    {
        return "namespace " + namespace_ + "\n{\n"
                + "    public class UnitTest_" + cls_.name() + "\n"
                + "    {\n"
                + "        [SetUp]\n"
                + "        public void SetUp()\n"
                + "        {\n"
                + "        }\n"
                + "    }\n"
                + "}";
    }
}
