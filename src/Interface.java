interface IInterface
{
    String toString();
}

public class Interface implements IInterface
{
    private String name_;

    Interface(String name)
    {
        name_ = name;
    }

    @Override
    public String toString()
    {
        return name_;
    }
}
