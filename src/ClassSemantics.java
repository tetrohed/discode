import java.util.Vector;

interface IClassSemantic
{
    String name();
    String openScope(Vector<IInterface> ifcs);
    String closeScope();
    String constructor(Vector<IDependency> dependencies);
}

class ClassSemantic implements IClassSemantic
{
    private final String name_;
    private String indentation = "    ";

    ClassSemantic(String name)
    {
        name_ = name;
    }

    @Override
    public String name()
    {
        return name_;
    }

    public String openScope(Vector<IInterface> ifcs)
    {
        StringBuilder res = new StringBuilder(indentation + "public " + name_);

        if (ifcs.size() > 0)
            res.append(" : ").append(ifcs.firstElement().toString());

        for (IInterface ifc: ifcs.subList(1, ifcs.size()))
        {
            res.append(", ");
            res.append(ifc.toString());
        }
        return res.append("\n").append(indentation).append("{\n").toString();
    }

    public String closeScope()
    {
        return "\n" + indentation + "}";
    }

    @Override
    public String constructor(Vector<IDependency> dependencies)
    {
        StringBuilder res = new StringBuilder(indentation + "public " + name_ + "(");

        if (dependencies.size() > 0)
            res.append(dependencies.firstElement().toString());

        for (IDependency dep: dependencies.subList(1, dependencies.size()))
        {
            res.append(", ");
            res.append(dep.toString());
        }

        return res.append(")").append("\n{\n}\n").toString();
    }
}


