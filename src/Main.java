import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

public class Main {

    public static void main(String[] args)
    {
        ClassSemantic cls = new ClassSemantic("Foo");
        UnitTest unitTest = new UnitTest("ns", cls);
        Module mdl = new Module("ns", cls);

        mdl.addInterface(new Interface("IBar"));
        mdl.addInterface(new Interface("IBarSecond"));
        mdl.addDependency(new Dependency(new Interface("IDep"), "dep"));
        mdl.addDependency(new Dependency(new Interface("IDepSecond"), "depSecond"));

        File file = new File(args[0]);
        File unitTest_file = new File(args[1]);

        try
        {
            PrintStream printStream = new PrintStream(file);
            PrintStream unitTest_printStream = new PrintStream(unitTest_file);

            printStream.print(mdl.toString());
            unitTest_printStream.print(unitTest.toString());
        }
        catch (FileNotFoundException e)
        {
            System.out.print(file.getAbsolutePath() + " was not found! ");
        }
    }
}

